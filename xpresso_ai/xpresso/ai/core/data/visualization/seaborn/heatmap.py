import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import os
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot
from xpresso.ai.core.data.visualization import utils


class NewPlot(Plot):
    """
    Generates a heatmap plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): data for plotting
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """
    def __init__(self, input_1, plot_title=None, output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH):
        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels
        self.plot = sns.heatmap(input_1, annot=True, cbar=False)
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL],
                      title=plot_title)

        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)
